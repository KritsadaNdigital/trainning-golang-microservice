package healthcheck

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/n-digital/trainning-golang-microservice/app/utils"
)

func (handler *handler) Health(c *gin.Context) {
	utils.JSONSuccessResponse(c, nil)
}
