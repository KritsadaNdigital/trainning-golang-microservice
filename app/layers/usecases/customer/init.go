package customer

import (
	"gitlab.com/n-digital/trainning-golang-microservice/app/entities"
	"gitlab.com/n-digital/trainning-golang-microservice/app/layers/repositories/customer"
)

type useCase struct {
	CustomerRepo customer.Repo
}

func InitUseCase(customerRepository customer.Repo) UseCase {
	return &useCase{
		CustomerRepo: customerRepository,
	}
}

// InitUseCase init auth use case
type UseCase interface {
	CreateUser(input *entities.Users) (*entities.Users, error)
}
